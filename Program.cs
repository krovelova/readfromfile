﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace WordsCounter
{
    class Program
    {
        // types
        // static variables
        // functions
        // instance variables

        enum EntityType
        {
            Invalid,
            None,
            Word,
            Number,
            HexaDecimalNumber
        }

        enum NumberVerificationStep
        {
            Invalid,
            None,
            Minus,
            FirstDigitZero,
            OtherDigits,
            Recognized
        }

        enum WordVerificationStep
        {
            Invalid,
            None,
            Letters,
            Recognized
        }

        enum SingleNameVerificationStep
        {
            Invalid,
            None,
            FirstBigLetter,
            MiddleLittleLetters,
            LastBigLetters
        }

        private enum StatisticsGatheringStatus
        {
            Ok,
            FileDoesNotExists,
            FileCannotBeOpened
        }

        private enum NamesCountingStatus
        {
            Ok,
            FileDoesntExist,
            FileCannotBeOpened
        }

        private class IntegerNumberRange
        {
            public static IntegerNumberRange RangeFirstNumberGiven(int firstNumber, int lastNumber)
            {
                return new IntegerNumberRange(firstNumber, firstNumber + lastNumber);
            }

            public IntegerNumberRange(int firstNumber, int lastNumber)
            {
                this.mFirstNumber = firstNumber;
                this.mLastNumber = lastNumber;
            }

            public bool IntegerRangeVerification()
            {
                if ((this.mFirstNumber < this.mLastNumber) || (this.lastNumberOfDigits > 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public bool Contains(int aNumber, bool aBorderValuesInclusive)
            {
                return aBorderValuesInclusive
                       ? mFirstNumber <= aNumber && aNumber <= mLastNumber // ако aBorderValuesInclusive е true
                       : mFirstNumber < aNumber && aNumber < mLastNumber;
            }

            public bool Contains(IntegerNumberRange that, bool aBorderValuesInclusive)// при дадено 1во и последно число 
            {
                return aBorderValuesInclusive
                       ? this.mFirstNumber <= that.mFirstNumber && that.mLastNumber <= this.mLastNumber
                       : this.mFirstNumber < that.mFirstNumber && that.mLastNumber < this.mLastNumber;
            }

            public void UniteWithRange(IntegerNumberRange that, bool aBorderValuesInclusive) //obedinqvane
            {
                if (this.mFirstNumber <= that.mFirstNumber)
                {
                    if ((that.mFirstNumber <= this.mLastNumber && aBorderValuesInclusive) || (that.mFirstNumber < this.mLastNumber && !aBorderValuesInclusive))
                    {
                        this.mLastNumber = that.mLastNumber;
                    }
                }
                else
                {
                    if ((this.mFirstNumber <= that.mLastNumber && aBorderValuesInclusive) || (this.mFirstNumber < that.mLastNumber && !aBorderValuesInclusive))
                    {
                        this.mFirstNumber = that.mFirstNumber;
                    }
                }
            }

            public void IntersectWithRange(IntegerNumberRange that, bool aBorderValuesInclusive) //sechenie
            {
                if (this.mFirstNumber <= that.mFirstNumber)
                {
                    if ((that.mFirstNumber <= this.mLastNumber - 1 && aBorderValuesInclusive) || (that.mFirstNumber < this.mLastNumber - 1 && !aBorderValuesInclusive))
                    {
                        this.mFirstNumber = that.mFirstNumber;
                    }
                }
                else
                {
                    if ((this.mFirstNumber <= that.mLastNumber - 1 && aBorderValuesInclusive) || (this.mFirstNumber < that.mLastNumber - 1 && !aBorderValuesInclusive))
                    {
                        this.mLastNumber = that.mLastNumber;
                    }
                }
            }

            private int mFirstNumber;
            private int mLastNumber;
            private int lastNumberOfDigits;

            public int getFirstDigit()
            {
                return this.mFirstNumber;
            }

            public int getLastDigit()
            {
                return this.mLastNumber;
            }

            public int getLastNumberOfDigits()
            {
                return this.lastNumberOfDigits;
            }
        }

        private struct EntityStatistics
        {
            public int wordsCount;
            public int integerNumbersCount;
            public int integerNumbersSum;
            public StatisticsGatheringStatus status;

            public void Statistics(int wordsCount, int integerNumbersCount, int integerNumbersSum)
            {
                this.wordsCount = wordsCount;
                this.integerNumbersCount = integerNumbersCount;
                this.integerNumbersSum = integerNumbersSum;
            }

            public void Statistics()
            {
                Statistics(0, 0, 0);
            }
        }

        private const uint LAST_BIG_LETTERS_EXPECTED_COUNT = 2;

        private static bool IsEntityDelimiter(char aSymbol)
        {
            return Char.IsWhiteSpace(aSymbol);
        }

        private static uint GetNamesCountFromFile(string aFilePath)
        {
            if (!File.Exists(aFilePath))
            {
                return 0;
            }

            StreamReader reader = null;
            try
            {
                reader = new StreamReader(aFilePath);
            }
            catch
            {
                return 0;
            }

            int lastBigLettersCount = 0;
            int nameCount = 0;

            SingleNameVerificationStep LettersVerification = SingleNameVerificationStep.None;

            while (!reader.EndOfStream)
            {
                char ch = (char)reader.Read();

                if (Char.IsLetter(ch))
                {
                    if (Char.IsUpper(ch))
                    {
                        if (LettersVerification == SingleNameVerificationStep.None)
                        {
                            LettersVerification = SingleNameVerificationStep.FirstBigLetter;
                        }
                        else if (LettersVerification == SingleNameVerificationStep.MiddleLittleLetters)
                        {
                            LettersVerification = SingleNameVerificationStep.LastBigLetters;

                            if (LettersVerification == SingleNameVerificationStep.LastBigLetters && lastBigLettersCount == 2)
                            {
                                LettersVerification = SingleNameVerificationStep.Invalid;
                            }

                            ++lastBigLettersCount;

                        }
                        else
                        {
                            LettersVerification = SingleNameVerificationStep.Invalid;
                        }
                    }
                    else
                    {
                        if (LettersVerification == SingleNameVerificationStep.FirstBigLetter)
                        {
                            LettersVerification = SingleNameVerificationStep.MiddleLittleLetters;
                        }
                        else
                        {
                            LettersVerification = SingleNameVerificationStep.Invalid; //  ако charLetterVerificationStep e None
                        }
                    }
                }
                else if (Char.IsWhiteSpace(ch))
                {
                    if (LettersVerification == SingleNameVerificationStep.LastBigLetters
                        && lastBigLettersCount == 2)
                    {
                        ++nameCount;
                    }

                    LettersVerification = SingleNameVerificationStep.None;
                    lastBigLettersCount = 0;
                }
                else
                {
                    LettersVerification = SingleNameVerificationStep.Invalid;
                }
            }
            if (reader != null)
            {
                reader.Close();
            }

            return (uint)nameCount;
        }

        // private static NamesCountingStatus GetNamesCountFromFile2(string aFilePath, ref uint aNamesCount)

        private static NamesCountingStatus GetNamesCountFromFile2(string aFilePath, ref uint namesCount)
        {
            if (!File.Exists(aFilePath))
            {
                return NamesCountingStatus.FileDoesntExist;
            }

            StreamReader reader = null;
            try
            {
                reader = new StreamReader(aFilePath);
            }
            catch
            {
                return NamesCountingStatus.FileCannotBeOpened;
            }

            uint lastBigLettersCount = 0;

            SingleNameVerificationStep singleNameVerificationStep = SingleNameVerificationStep.None;
            while (!reader.EndOfStream)
            {
                char c = (char)reader.Read();

                switch (singleNameVerificationStep)
                {
                    case SingleNameVerificationStep.None:
                        if (Char.IsUpper(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.FirstBigLetter;

                            break;
                        }

                        if (Char.IsWhiteSpace(c))
                        {
                            break;
                        }

                        singleNameVerificationStep = SingleNameVerificationStep.Invalid;
                        break;

                    case SingleNameVerificationStep.FirstBigLetter:
                        if (Char.IsLower(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.MiddleLittleLetters;
                            break;
                        }

                        if (Char.IsWhiteSpace(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.None;
                            break;
                        }

                        singleNameVerificationStep = SingleNameVerificationStep.Invalid;
                        break;

                    case SingleNameVerificationStep.MiddleLittleLetters:
                        if (Char.IsUpper(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.LastBigLetters;
                            ++lastBigLettersCount;
                            break;
                        }

                        if (Char.IsWhiteSpace(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.None;
                            break;
                        }

                        singleNameVerificationStep = SingleNameVerificationStep.Invalid;
                        break;

                    case SingleNameVerificationStep.LastBigLetters:
                        if (Char.IsUpper(c))
                        {
                            if (lastBigLettersCount == LAST_BIG_LETTERS_EXPECTED_COUNT)
                            {
                                singleNameVerificationStep = SingleNameVerificationStep.Invalid;
                                break;
                            }

                            ++lastBigLettersCount;
                            break;
                        }

                        if (Char.IsWhiteSpace(c))
                        {
                            if (lastBigLettersCount == LAST_BIG_LETTERS_EXPECTED_COUNT)
                            {
                                ++namesCount;
                            }

                            lastBigLettersCount = 0;
                            singleNameVerificationStep = SingleNameVerificationStep.None;
                            break;
                        }

                        singleNameVerificationStep = SingleNameVerificationStep.Invalid;
                        lastBigLettersCount = 0;
                        break;

                    case SingleNameVerificationStep.Invalid:
                        if (Char.IsWhiteSpace(c))
                        {
                            singleNameVerificationStep = SingleNameVerificationStep.None;
                        }

                        break;
                }
            }

            return NamesCountingStatus.Ok;
        }

        public interface EntityRecognizer
        {
            void Clear();
            bool Verify(char aSymbol);
            bool HasFinished();
        }

        private class NumberRecognizer : EntityRecognizer
        {
            public NumberRecognizer()
            {
                Clear();
            }

            public bool HasFinished()
            {
                return m_integerVerificationStep == NumberVerificationStep.Recognized
                       || m_integerVerificationStep == NumberVerificationStep.Invalid;
            }

            public void Clear()
            {
                m_integerVerificationStep = NumberVerificationStep.None;
            }

            public bool Verify(char ch)
            {
                switch (m_integerVerificationStep)
                {
                    case NumberVerificationStep.None:
                        if (ch == '-')
                        {
                            m_integerVerificationStep = NumberVerificationStep.Minus;
                            return true;
                        }

                        if (Char.IsNumber(ch))
                        {
                            if (ch == '0')
                            {
                                m_integerVerificationStep = NumberVerificationStep.FirstDigitZero;
                                return true;
                            }

                            m_integerVerificationStep = NumberVerificationStep.OtherDigits;
                            return true;
                        }

                        if (IsPureHexDigit(ch))
                        {
                            m_integerVerificationStep = NumberVerificationStep.OtherDigits;
                            return true;
                        }

                        m_integerVerificationStep = NumberVerificationStep.Invalid;
                        return false;

                    case NumberVerificationStep.Minus:
                        if (Char.IsNumber(ch))
                        {
                            if (ch == '0')
                            {
                                m_integerVerificationStep = NumberVerificationStep.Invalid;
                                return false;
                            }

                            m_integerVerificationStep = NumberVerificationStep.OtherDigits;
                            return true;
                        }

                        if (IsPureHexDigit(ch))
                        {
                            m_integerVerificationStep = NumberVerificationStep.OtherDigits;
                            return true;
                        }

                        m_integerVerificationStep = NumberVerificationStep.Invalid;
                        return false;

                    case NumberVerificationStep.FirstDigitZero:
                        if (Char.IsWhiteSpace(ch))
                        {
                            m_integerVerificationStep = NumberVerificationStep.Recognized;
                            break;
                        }

                        m_integerVerificationStep = NumberVerificationStep.Invalid;
                        return false;

                    case NumberVerificationStep.OtherDigits:
                        if (Char.IsNumber(ch))
                        {
                            return true;
                        }

                        if (IsPureHexDigit(ch))
                        {
                            return true;
                        }

                        if (Char.IsWhiteSpace(ch))
                        {
                            m_integerVerificationStep = NumberVerificationStep.Recognized;
                            break;
                        }
                        
                        m_integerVerificationStep = NumberVerificationStep.Invalid;
                        return false;

                    case NumberVerificationStep.Recognized:
                        m_integerVerificationStep = NumberVerificationStep.None;
                        return true;

                    case NumberVerificationStep.Invalid:
                        m_integerVerificationStep = NumberVerificationStep.None;
                        return false;
                }

                return true;
            }

            private static bool IsPureHexDigit(char aSymbol)
            {
                return (aSymbol >= 'a' && aSymbol <= 'f')
                       || (aSymbol >= 'A' && aSymbol <= 'F');
            }

            private NumberVerificationStep m_integerVerificationStep;
        }

        private class WordRecognizer : EntityRecognizer
        {
            public WordRecognizer()
            {
                Clear();
            }

            public bool HasFinished()
            {
                return m_wordVerificationStep == WordVerificationStep.Recognized
                       || m_wordVerificationStep == WordVerificationStep.Invalid;
            }

            public void Clear()
            {
                m_wordVerificationStep = WordVerificationStep.None;
                m_lettersCount = 0;
            }

            public bool Verify(char ch)
            {
                bool verified = VerifyInternal(ch);
                if (verified)
                {
                    ++m_lettersCount;
                }

                return verified;
            }

            private bool VerifyInternal(char ch)
            {
                switch (m_wordVerificationStep)
                {
                    case WordVerificationStep.None:
                        if (Char.IsLetter(ch))
                        {
                            m_wordVerificationStep = WordVerificationStep.Letters;
                            return true;
                        }

                        m_wordVerificationStep = WordVerificationStep.Invalid;
                        return false;

                    case WordVerificationStep.Letters:
                        if (Char.IsLetter(ch))
                        {
                            return true;
                        }

                        if (Char.IsWhiteSpace(ch))
                        {
                            
                            m_wordVerificationStep = m_lettersCount >= 2
                                                     ? WordVerificationStep.Recognized
                                                     : WordVerificationStep.Invalid;

                            /*
                            if (m_lettersCount >= 2)
                            {
                                m_wordVerificationStep = WordVerificationStep.Recognized;
                                return true;
                            }
                            m_wordVerificationStep = WordVerificationStep.Invalid;
                            return false;
                            */
                            break;
                        }

                        m_wordVerificationStep = WordVerificationStep.Invalid;
                        return false;

                    case WordVerificationStep.Recognized:
                        m_wordVerificationStep = WordVerificationStep.None;
                        return true;

                    case WordVerificationStep.Invalid:
                        m_wordVerificationStep = WordVerificationStep.None;
                        return false;
                }

                return true;
            }

            private WordVerificationStep m_wordVerificationStep;
            private int m_lettersCount;
        }

        private static Dictionary<EntityType, EntityRecognizer> CreateEntityRecognizers()
        {
            return new Dictionary<EntityType, EntityRecognizer>
            {
                { EntityType.Number, new NumberRecognizer() },
                { EntityType.Word, new WordRecognizer() }
            };
        }

        private static void AccountEntity(
            ref EntityStatistics aFileStatistics,
            EntityType aEntityType,
            String aEntityRaw)
        {
            switch (aEntityType)
            {
                case EntityType.Number:
                    ++aFileStatistics.integerNumbersCount;
                    aFileStatistics.integerNumbersSum += int.Parse(aEntityRaw,
                                                                  Regex.IsMatch(aEntityRaw, @"[a-fA-F]+")
                                                                ? System.Globalization.NumberStyles.HexNumber
                                                                : System.Globalization.NumberStyles.Integer);
                    break;

                case EntityType.Word:
                    ++aFileStatistics.wordsCount;
                    break;
            }
        }

        private static EntityStatistics GetFileStatistics(string aFilePath)
        {
            EntityStatistics fileStatistics = new EntityStatistics();

            if (!File.Exists(aFilePath))
            {
                fileStatistics.status = StatisticsGatheringStatus.FileDoesNotExists;
                return fileStatistics;
            }

            StreamReader reader = null;
            try
            {
                reader = new StreamReader(aFilePath);
            }
            catch
            {
                fileStatistics.status = StatisticsGatheringStatus.FileCannotBeOpened;
                return fileStatistics;
            }

            StringBuilder entityRaw = new StringBuilder();
            EntityType recognizedEntity = EntityType.None;
            Dictionary<EntityType, EntityRecognizer> recognizers = CreateEntityRecognizers();
            Dictionary<EntityType, EntityRecognizer>.Enumerator recognizerIterator = recognizers.GetEnumerator();
            int k = 0;

            do
            {
                char ch = reader.EndOfStream
                          ? ' '
                          : (char)reader.Read();
                entityRaw.Append(ch);

                while (k < entityRaw.Length)
                {
                    if (recognizedEntity == EntityType.None)
                    {
                        recognizerIterator = recognizers.GetEnumerator();
                        while (recognizerIterator.MoveNext())
                        {
                            EntityRecognizer recognizerTester = recognizerIterator.Current.Value;
                            if (recognizerTester.Verify(entityRaw[k]))
                            {
                                recognizedEntity = recognizerIterator.Current.Key;
                                break;
                            }

                            recognizerTester.Clear();
                        }

                        if (recognizedEntity == EntityType.None
                            && !IsEntityDelimiter(entityRaw[k]))
                        {
                            recognizedEntity = EntityType.Invalid;
                        }

                        ++k;
                        break;
                    }

                    if (recognizedEntity == EntityType.Invalid)
                    {
                        if (IsEntityDelimiter(entityRaw[k]))
                        {
                            recognizedEntity = EntityType.None;
                            entityRaw.Clear();
                            k = 0;
                            break;
                        }

                        ++k;
                        break;
                    }

                    //word , number recognizer
                    EntityRecognizer recognizer = recognizers[recognizedEntity]; //EntityRecognizer interface type, recognizers - Dictionary, recognizedEntity - (None,Invalid,Word,Number)
                    if (!recognizer.Verify(entityRaw[k]))
                    {
                        k = 0;

                        if (recognizerIterator.MoveNext())
                        {
                            recognizedEntity = recognizerIterator.Current.Key;
                            continue;
                        }

                        recognizedEntity = EntityType.Invalid;
                        recognizer.Clear();
                        break;
                    }

                    if (recognizer.HasFinished())
                    {
                        //string entityRawStr = entityRaw.ToString().TrimEnd(entityRaw[entityRaw.Length - 1]);
                        AccountEntity(ref fileStatistics, recognizedEntity, entityRaw.ToString());
                        entityRaw.Clear();
                        recognizer.Clear();
                        recognizedEntity = EntityType.None;
                        k = 0;
                        break;
                    }

                    ++k;
                }
            }
            while (!reader.EndOfStream);

            if (reader != null)
            {
                reader.Close();
            }

            return fileStatistics;
            // return wordsCount, numbersSum, nNumbersCount;
        }

        static void Main(string[] args)
        {
            /*
            int firstDigit = Console.Read();
            int lastDigit = Console.Read();
            int lastNumberOfDigits = Console.Read();
            int num = Console.Read();

            IntegerNumberRange r1 = new IntegerNumberRange(2, 5);
            IntegerNumberRange r2 = new IntegerNumberRange(3, 7);

            IntegerNumberRange r3 = IntegerNumberRange.RangeFirstNumberGiven(2, 8);

            bool verification = r1.IntegerRangeVerification();
            if (verification == false)
            {
                Console.Error.WriteLine("The range is incorrect.");
            }

            r1.Contains(r2, true);
            r1.UniteWithRange(r2, true);
            r1.getFirstDigit();
            */

            if (args.Length != 1)
            {
                Console.WriteLine("Please specify a single file path argument.");
                return;
            }

            string filePath = args[0];
            EntityStatistics result = GetFileStatistics(filePath); //в result oт тип FileStatistics приема връщания резултат на метода  

            //uint namesCount = 0;
            //NamesCountingStatus result2 = GetNamesCountFromFile2(filePath, ref namesCount);

            if (result.status == StatisticsGatheringStatus.FileDoesNotExists)
            {
                Console.Error.WriteLine("File path is incorrect or the file pointed to by the path does not exist.");
            }
            else if (result.status == StatisticsGatheringStatus.FileCannotBeOpened)
            {
                Console.Error.WriteLine("The file cannot be opened.");
            }
            else
            {
                Console.WriteLine("The file has {0} words, {1} numbers and the sum of the numbers is {2}", result.wordsCount, result.integerNumbersCount, result.integerNumbersSum);
            }
        }
    }
}

